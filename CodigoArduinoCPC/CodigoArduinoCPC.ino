#include <PID_v1.h>

#define PIN_INPUT A1
#define PIN_SETPOINT A0
#define PIN_OUTPUT 3

unsigned long lastSend = 0;

//Define Variables we'll be connecting to
double Setpoint, Input, Output;

//Specify the links and initial tuning parameters
double Kp=0.15, Ki=0.8, Kd=0.0;
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

void setup()
{
  Serial.begin(115200);
  //initialize the variables we're linked to 
  Input = analogRead(PIN_INPUT);
  Setpoint = 100;

  //turn the PID on 
  myPID.SetMode (AUTOMATIC);
}

void loop()
{
  Input = analogRead(PIN_INPUT);
  Setpoint = analogRead(PIN_SETPOINT);
  myPID.Compute();
  analogWrite(PIN_OUTPUT, Output);

  if (millis()-lastSend > 100) { 
    lastSend = millis();
  
    Serial.print (Setpoint); 
    Serial.print(""); 
    Serial.print(Input); 
    Serial.print(" "); 
    Serial.print (Output);
    Serial.println(" ");
  }
}
